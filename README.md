# README #

This is a screenshot tool for iOS devices.

### How do I get set up? ###

* Choose your device from [devices.css](http://marvelapp.github.io/devices.css/)
* Update the index.html for preferred device.
* add your screenshot in root folder as `screenshot.jpg`
* take a screenshot and paste it in gimp/photoshop to trim the edges.
* Export the image in preferred format.

### Who do I talk to? ###

* Repo Admins
* Akshay Darekar
